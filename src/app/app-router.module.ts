//Dependencies
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';

//Assets
import { FilmsComponent } from './components/films/films.component';
import { CharactersComponent } from './components/characters/characters.component';
import { Page404Component } from './components/page404/page404.component';
import { CharactersGuard } from './components/characters/characters.guard';

const routes: Routes = [
  {
    path: 'personajes/:film',
    component: CharactersComponent,
    canActivate: [CharactersGuard]
  },
  {
    path: '',
    pathMatch: 'full' ,
    component: FilmsComponent,
    data: {
      title: 'Star Wars',
    }
  },
  {
    path: '**',
    component: Page404Component
  }
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes)
  ],
  exports: [
    RouterModule
  ]
})
export class AppRouterModule { }
