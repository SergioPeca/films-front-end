//Dependencies
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';

//Assets
import { AppComponent } from './components/app/app.component';
import { FilmsComponent } from './components/films/films.component';
import { CharactersComponent } from './components/characters/characters.component';
import { CharacterInfoComponent } from './components/characters/character-info/character-info.component';
import { Page404Component } from './components/page404/page404.component';
import { HeaderComponent } from './components/commons/header/header.component';
import { FilmsService } from './services/films/films.service';
import { CharactersService } from './services/characters/characters.service';
import { TransformerService } from './model/transformer.service';
import { AppRouterModule } from './app-router.module';

@NgModule({
  declarations: [
    AppComponent,
    FilmsComponent,
    CharactersComponent,
    CharacterInfoComponent,
    Page404Component,
    HeaderComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpClientModule,
    AppRouterModule
  ],
  providers: [
    FilmsService,
    CharactersService,
    TransformerService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
