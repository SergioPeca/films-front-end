//Dependencies
import { Component, Input } from '@angular/core';

@Component({
  selector: 'characterInfo',
  templateUrl: './character-info.component.html',
  styleUrls: ['./character-info.component.css']
})
export class CharacterInfoComponent {
  @Input('characters') characters: string[];
  private __proto__: string;

  constructor(){}

  ngOnInit(){
    this.validateIfRequired(this.getRequiredParams());
  }

  ngOnChanges(){
    this.validateIfRequired(this.getRequiredParams());
  }

  getRequiredParams(){
    let requiredParams = [{
      "inputName": 'characters',
      "value": this.characters
    }];
    return requiredParams;
  }

  validateIfRequired(inputFields: any[]){
    inputFields.map((input) => {
      if(!input.value) throw new Error(`[ERROR in ${this.__proto__.constructor.name}] El campo ${input.inputName} es requerido!`);
    });
  }

}
