//Dependencies
import { Component } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { forkJoin } from 'rxjs';

//Assets
import { CharactersService } from '../../services/characters/characters.service';
import { TransformerService } from '../../model/transformer.service';

@Component({
  selector: 'characters',
  templateUrl: './characters.component.html',
  styleUrls: ['./characters.component.css']
})
export class CharactersComponent {
  private headerTitle = "Star Wars";
  private filmTitle: string;
  private characters: string[];
  private loading: boolean;

  constructor(private route: ActivatedRoute,
    private charactersService: CharactersService,
    private transformerService: TransformerService){
        this.characters = [];
        this.loading = true;
  }

  ngOnInit(){
    this.initServiceData();
    this.initParamData();
  }

  initParamData(){
    this.route.params.subscribe(value => {
      this.filmTitle = value.film;
    });
  }

  initServiceData(){
    let apiCharactersObs = this.charactersService.getApiStarWarsCharacters();
    forkJoin(apiCharactersObs).subscribe((response) => {
      this.characters = this.transformerService.transformToCharacterObj(response);
      this.loading = false;
    }, (error) => {
      console.log("ERROR --> ", error);
    });
  }

}
