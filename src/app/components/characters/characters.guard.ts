//Dependencies
import { Injectable } from '@angular/core';
import { CanActivate, Router, ActivatedRoute } from '@angular/router';

//Assets
import { CharactersService } from '../../services/characters/characters.service';

@Injectable({
    providedIn: 'root'
})
export class CharactersGuard implements CanActivate {
  constructor(
    private charactersService: CharactersService,
    private route: ActivatedRoute,
    private router: Router){}

  canActivate(){
    if(this.charactersService.getStarWarsCharacters() === undefined) {
      this.router.navigate([''], {relativeTo: this.route});
      return false;
    } else {
      return true;
    }
  }
}
