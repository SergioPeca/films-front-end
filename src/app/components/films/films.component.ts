//Dependencies
import { Component } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

//Assets
import { HeaderComponent } from '../../components/commons/header/header.component';
import { FilmsService } from '../../services/films/films.service';
import { CharactersService } from '../../services/characters/characters.service';
import { TransformerService } from '../../model/transformer.service';
import { Film } from '../../model/objs/Film';

@Component({
  selector: 'films',
  templateUrl: './films.component.html',
  styleUrls: ['./films.component.css']
})
export class FilmsComponent {
  films: Film[];
  loading: boolean;
  headerTitle: string;

  constructor(private transformerService: TransformerService,
    private filmsService: FilmsService,
    private charactersService: CharactersService,
    private route: ActivatedRoute,
    private router: Router){
    this.films = [];
    this.loading = true;
  }

  ngOnInit(){
    this.initParamData();
    this.getFilms();
  }

  initParamData(){
    this.route.data.subscribe(value => {
      this.headerTitle = value.title;
    });
  }

  getFilms(){
    this.filmsService.getStarWarsFilms().subscribe((res: any) => {
      let results = res.results;
      for(let index in results ){
        let obj = this.transformerService.transformToFilmObj(results[index]);
        this.films.push(obj);
      }
      this.films.sort((f1, f2) => f1.getEpisodeId() - f2.getEpisodeId());
      this.loading = false;
    });
  }

  redirect(characters: string[], filmTitle: string){
    this.charactersService.setCharacters(characters);
    this.router.navigate(['/personajes', filmTitle], {relativeTo: this.route});
    //this.router.navigateByUrl('personajes/' + filmTitle);
  }
}
