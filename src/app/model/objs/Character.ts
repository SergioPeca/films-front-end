export class Character {
  private name: string;
  private gender: string;
  private birthYear: string;
  private height: string;
  private hairColor: string;
  private skinColor: string;
  private eyeColor: string;
  private films: string[];

  constructor(nameN: string, genderN: string,
    birthYearN: string, heightN: string,
    hairColorN: string, skinColorN: string,
    eyeColorN: string, filmsN: string[]){
      this.name = nameN;
      this.gender = genderN;
      this.birthYear = birthYearN;
      this.height = heightN;
      this.hairColor = hairColorN;
      this.skinColor = skinColorN;
      this.eyeColor = eyeColorN;
      this.films = filmsN;
  }

}
