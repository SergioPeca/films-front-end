export class Film {
    private title: string;
    private episodeId: number;
    private director: string;
    private characters: string[];

    constructor(title: string, episodeId: number, director: string, characters: string[]) {
      this.title = title;
      this.episodeId = episodeId;
      this.director = director;
      this.characters = characters;
    }

    getEpisodeId(){
      return this.episodeId;
    }

    getCharacters(){
      return this.characters;
    }
}
