//Dependencies
import { Injectable } from '@angular/core';

//Assets
import { Film } from './objs/Film';
import { Character } from './objs/Character';

@Injectable()
export class TransformerService {

  constructor () {}

  transformToFilmObj(filmResponse){
    return new Film(
      filmResponse.title,
      filmResponse.episode_id,
      filmResponse.director,
      filmResponse.characters
    );
  }

  transformToCharacterObj(charactersResponse){
    console.log(charactersResponse);
    return charactersResponse.map((character) => {
      return new Character(
        character.name,
        character.gender,
        character.birth_year,
        character.height,
        character.hair_color,
        character.skin_color,
        character.eye_color,
        character.films)
    });
  }

}
