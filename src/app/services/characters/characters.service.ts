//Dependencies
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable()
export class CharactersService {

  private starWarsCharactersUrl: string[];

  constructor (private http: HttpClient) {
    this.starWarsCharactersUrl = undefined;
  }

  setCharacters(starWarsCharactersUrlN: string[]){
    this.starWarsCharactersUrl = starWarsCharactersUrlN;
  }

  getStarWarsCharacters(){
    return this.starWarsCharactersUrl;
  }

  getApiStarWarsCharacters(){
    return this.starWarsCharactersUrl.map((url) => {
      return this.http.get(url);
    });
  }

}
